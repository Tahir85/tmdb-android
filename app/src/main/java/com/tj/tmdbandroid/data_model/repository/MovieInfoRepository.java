package com.tj.tmdbandroid.data_model.repository;

import android.arch.lifecycle.MutableLiveData;

import com.tj.tmdbandroid.data_model.data.movie_info.MovieInfo;
import com.tj.tmdbandroid.executor.AppExecutors;
import com.tj.tmdbandroid.network.NetworkState;
import com.tj.tmdbandroid.network.Webservice;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class MovieInfoRepository extends Repository {

    private MutableLiveData<MovieInfo> movieInfoData;

    @Inject
    public MovieInfoRepository(AppExecutors executors, Webservice webservice){
        this.executors = executors;
        this.webservice = webservice;
        this.movieInfoData = new MutableLiveData<>();
    }

    public MutableLiveData<MovieInfo> getMovieInfo(int id){

        setNetworkState(NetworkState.LOADING);

        this.executors.networkIO().execute(() -> {
            this.webservice.getMovieInfo(id).enqueue(new Callback<MovieInfo>() {
                @Override
                public void onResponse(Call<MovieInfo> call, Response<MovieInfo> response) {
                    if(response.isSuccessful()) {

                        MovieInfo info = response.body();
                        movieInfoData.postValue(info);

                        setNetworkState(NetworkState.COMPLETED);

                    }else{
                        setNetworkState((new NetworkState(NetworkState.Status.STATE_FAILED,
                                "API Error: " + response.message())));
                    }
                }

                @Override
                public void onFailure(Call<MovieInfo> call, Throwable t) {
                    String errorMessage = t == null ? "unknown error" : t.getMessage();
                    setNetworkState((new NetworkState(NetworkState.Status.STATE_FAILED, errorMessage)));
                }
            });
        });

        return movieInfoData;
    }

}
