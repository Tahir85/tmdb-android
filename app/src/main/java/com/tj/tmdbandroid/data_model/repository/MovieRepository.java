package com.tj.tmdbandroid.data_model.repository;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import com.tj.tmdbandroid.data_model.data.discover.Movie;
import com.tj.tmdbandroid.data_model.data.paging.DataFactory;
import com.tj.tmdbandroid.executor.AppExecutors;
import com.tj.tmdbandroid.network.Webservice;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MovieRepository extends Repository{

    private LiveData<PagedList<Movie>> moviesLiveData;

    public String filterString;
    private DataFactory dataFactory;

    @Inject
    public MovieRepository(AppExecutors appExecutors, Webservice webservice){

        this.executors = appExecutors;
        this.webservice = webservice;
        initDataSource();
    }

    /*
     * Setup data source for paging lib.
     */
    private void initDataSource(){

        dataFactory = new DataFactory(this, ()->{
            //reinit data source on invalidate
            initDataSource();
        });

        PagedList.Config pagedListConfig =
                (new PagedList.Config.Builder())
                        .setEnablePlaceholders(false)
                        .setInitialLoadSizeHint(20)
                        .setPageSize(20)
                        .build();

        moviesLiveData =  (new LivePagedListBuilder(dataFactory, pagedListConfig))
                .setFetchExecutor(this.executors.networkIO())
                .build();

    }

    /*
     * Getter method for the pageList
     */
    public LiveData<PagedList<Movie>> getMoviesLiveData() {
        return moviesLiveData;
    }

    /*
    * Filtered movie discovery
    * */
    public void filter(String filter){

        //have the new filter
        this.filterString = filter;

        //invalidate previous data source
        dataFactory.getFeedDataSource().getValue().invalidate();

    }


    /*
    * Clear filter and fetch a fresh
    * */
    public boolean clearFilter(){
        if (this.filterString != null) {
            filter(null);
            return true;
        }
        return false;
    }
}
