package com.tj.tmdbandroid.data_model.data.paging;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.tj.tmdbandroid.data_model.data.discover.DiscoverResponse;
import com.tj.tmdbandroid.data_model.data.discover.Movie;
import com.tj.tmdbandroid.data_model.repository.MovieRepository;
import com.tj.tmdbandroid.injection.modules.NetModule;
import com.tj.tmdbandroid.network.NetworkState;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviesDataSource extends PageKeyedDataSource<Long, Movie> {

    private static final String TAG = MoviesDataSource.class.getSimpleName();

    private MovieRepository repository;
    private String filterOptions;

    public MoviesDataSource(MovieRepository repository) {
        this.repository = repository;
        this.filterOptions = repository.filterString;
    }


    @Override
    public void loadInitial(@NonNull LoadInitialParams<Long> params,
                            @NonNull LoadInitialCallback<Long, Movie> callback) {

        repository.setNetworkState(NetworkState.LOADING);

        String url = NetModule.BASE_URL + "discover/movie?include_video=true&page=1";
        if (filterOptions != null){
            url = url + filterOptions;
        }
        this.repository.webservice.discoverMovies(url).enqueue(new Callback<DiscoverResponse>() {
            @Override
            public void onResponse(Call<DiscoverResponse> call, Response<DiscoverResponse> response) {
                if(response.isSuccessful()) {

                    List<Movie> movieList = response.body().getResults();

                    callback.onResult(movieList, null, 2l);

                    repository.setNetworkState(NetworkState.COMPLETED);

                }else{
                    repository.setNetworkState((new NetworkState(NetworkState.Status.STATE_FAILED,
                            "API Error: " + response.message())));
                }
            }

            @Override
            public void onFailure(Call<DiscoverResponse> call, Throwable t) {
                String errorMessage = t == null ? "unknown error" : t.getMessage();

                repository.setNetworkState((new NetworkState(NetworkState.Status.STATE_FAILED, errorMessage)));

            }
        });

    }

    @Override
    public void loadBefore(@NonNull LoadParams<Long> params,
                           @NonNull LoadCallback<Long, Movie> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Long> params,
                          @NonNull LoadCallback<Long, Movie> callback) {

        repository.setNetworkState(NetworkState.LOADING);
        String url = NetModule.BASE_URL + "discover/movie?include_video=true&page="+params.key;
        if (filterOptions != null){
            url = url + filterOptions;
        }
        this.repository.webservice.discoverMovies(url).enqueue(new Callback<DiscoverResponse>() {
            @Override
            public void onResponse(Call<DiscoverResponse> call, Response<DiscoverResponse> response) {
                if(response.isSuccessful()) {

                    List<Movie> movieList = response.body().getResults();

                    long nextKey = response.body().getPage();
                    callback.onResult(movieList, nextKey);

                    repository.setNetworkState((NetworkState.COMPLETED));

                }else{
                    repository.setNetworkState((new NetworkState(NetworkState.Status.STATE_FAILED,
                            "API Error: " + response.message())));
                }
            }

            @Override
            public void onFailure(Call<DiscoverResponse> call, Throwable t) {
                String errorMessage = t == null ? "unknown error" : t.getMessage();

                repository.setNetworkState((new NetworkState(NetworkState.Status.STATE_FAILED, errorMessage)));

            }
        });
    }
}