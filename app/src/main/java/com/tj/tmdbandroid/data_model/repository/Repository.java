package com.tj.tmdbandroid.data_model.repository;

import android.arch.lifecycle.MutableLiveData;

import com.tj.tmdbandroid.executor.AppExecutors;
import com.tj.tmdbandroid.network.NetworkState;
import com.tj.tmdbandroid.network.Webservice;

public abstract class Repository {

    public Webservice webservice;
    protected AppExecutors executors;

    protected MutableLiveData<NetworkState> networkState = new MutableLiveData<>();

    public MutableLiveData<NetworkState> getNetworkState() {
        return networkState;
    }

    public void setNetworkState(NetworkState networkState) {
        this.networkState.postValue(networkState);
    }

    protected void networkStarted(){
        networkState.postValue(NetworkState.LOADING);
    }

    protected void networkCompletd(){
        networkState.postValue(NetworkState.COMPLETED);
    }

    protected void networkError(String error){
        networkState.postValue((new NetworkState(NetworkState.Status.STATE_FAILED, error)));
    }

}
