package com.tj.tmdbandroid.data_model.data.paging;

import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.tj.tmdbandroid.data_model.repository.MovieRepository;

public class DataFactory extends DataSource.Factory {

    private MutableLiveData<MoviesDataSource> feedLiveDataSource;

    private MovieRepository repository;
    private DataSource.InvalidatedCallback onInvalidatedCallback;

    public DataFactory(MovieRepository repository, DataSource.InvalidatedCallback onInvalidatedCallback) {
        this.repository = repository;
        this.onInvalidatedCallback = onInvalidatedCallback;
    }

    @Override
    public DataSource create() {
        MoviesDataSource source = new MoviesDataSource(repository);
        //source.addInvalidatedCallback(onInvalidatedCallback);
        feedLiveDataSource = new MutableLiveData<>();
        feedLiveDataSource.postValue(source);
        return source;
    }

    public MutableLiveData<MoviesDataSource> getFeedDataSource() {
        return feedLiveDataSource;
    }
}
