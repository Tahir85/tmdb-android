package com.tj.tmdbandroid.network;

import com.tj.tmdbandroid.data_model.data.discover.DiscoverResponse;
import com.tj.tmdbandroid.data_model.data.movie_info.MovieInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface Webservice {

    //@GET("discover/movie")
    //Call<DiscoverResponse> discoverMovies(@Query("include_video") boolean includeVideo, @Query("page") int page);

    @GET
    Call<DiscoverResponse> discoverMovies(@Url String url);

    @GET("movie/{id}")
    Call<MovieInfo> getMovieInfo(@Path("id") int id);

}
