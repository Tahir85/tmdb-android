package com.tj.tmdbandroid.vm;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.tj.tmdbandroid.data_model.data.movie_info.MovieInfo;
import com.tj.tmdbandroid.data_model.repository.MovieInfoRepository;

import javax.inject.Inject;

public class MovieInfoViewModel extends ViewModel {

    private MovieInfoRepository movieInfoRepository;

    @Inject
    public MovieInfoViewModel(MovieInfoRepository repository){
        this.movieInfoRepository = repository;
    }

    public MutableLiveData<MovieInfo> getMovieInfo(int id){
        return this.movieInfoRepository.getMovieInfo(id);
    }
}
