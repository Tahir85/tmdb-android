package com.tj.tmdbandroid.vm;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;

import com.tj.tmdbandroid.data_model.data.discover.Movie;
import com.tj.tmdbandroid.data_model.repository.MovieRepository;
import com.tj.tmdbandroid.network.NetworkState;

import javax.inject.Inject;

public class MoviesViewModel extends ViewModel {

    private MovieRepository movieRepository;

    @Inject
    public MoviesViewModel(MovieRepository movieRepository){
        this.movieRepository = movieRepository;
    }


    /*
     * Getter method for the network state
     */
    public LiveData<NetworkState> getNetworkState() {
        return this.movieRepository.getNetworkState();
    }

    /*
     * Getter method for the pageList
     */
    public LiveData<PagedList<Movie>> getMoviesLiveData() {
        return this.movieRepository.getMoviesLiveData();
    }

    public void filter(String filter) {
        this.movieRepository.filter(filter);
    }

    public boolean clearFilter(){
        return this.movieRepository.clearFilter();
    }
}
