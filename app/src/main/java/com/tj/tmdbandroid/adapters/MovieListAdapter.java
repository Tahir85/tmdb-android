package com.tj.tmdbandroid.adapters;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.tj.tmdbandroid.data_model.data.discover.Movie;
import com.tj.tmdbandroid.databinding.FeedItemBinding;
import com.tj.tmdbandroid.network.NetworkState;
import com.tj.tmdbandroid.ui.MovieInfoActivity;

public class MovieListAdapter extends PagedListAdapter<Movie, RecyclerView.ViewHolder> {

    private static final int TYPE_PROGRESS = 0;
    private static final int TYPE_ITEM = 1;

    private Context context;
    private NetworkState networkState;
    public MovieListAdapter(Context context) {
        super(Movie.DIFF_CALLBACK);
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        FeedItemBinding itemBinding = FeedItemBinding.inflate(layoutInflater, parent, false);
        MovieItemViewHolder viewHolder = new MovieItemViewHolder(itemBinding);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((MovieItemViewHolder)holder).bindTo(getItem(position));
    }


    public class MovieItemViewHolder extends RecyclerView.ViewHolder {

        private FeedItemBinding binding;
        public MovieItemViewHolder(FeedItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindTo(final Movie movie) {

            Picasso.get().load(movie.getPosterURL()).into(binding.imageView);
            binding.textView.setText(movie.getTitle());

            binding.imageView.setOnClickListener(v -> {

                Context context = binding.getRoot().getContext();
                Intent intent = MovieInfoActivity.createIntent(movie.getId(), context);
                context.startActivity(intent);

            });
        }
    }
}
