package com.tj.tmdbandroid.injection.modules;
import com.google.gson.Gson;
import com.tj.tmdbandroid.App;
import com.tj.tmdbandroid.BuildConfig;
import com.tj.tmdbandroid.network.Webservice;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    private static final String TAG = NetModule.class.getSimpleName();

    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String API_KEY = "3fddadcca763b200391636829508abbc";

    @Provides
    @Singleton
    public GsonConverterFactory provideConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    Cache provideHttpCache() {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(App.getInstance().getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Cache cache) {

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(50, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS)
                .cache(cache);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        //send api_key in every outgoing request. language is optional
        builder.addInterceptor(chain -> {
            Request request = chain.request();
            HttpUrl.Builder newBuilder = request.url().newBuilder();
            HttpUrl url = newBuilder
                    .addQueryParameter("api_key", API_KEY)
                    .addQueryParameter("language", "en-US")
                    .build();
            request = request.newBuilder().url(url).build();
            return chain.proceed(request);
        });


        return builder.build();
    }


    @Provides
    @Singleton
    public Retrofit provideRetrofit2(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory) {

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
                .build();
    }

    @Provides
    @Singleton
    public Webservice providesApiInterface(Retrofit retrofit) {
        return retrofit.create(Webservice.class);
    }

    @Provides
    @Singleton
    public Gson provideGson(){
        return new Gson();
    }

}

