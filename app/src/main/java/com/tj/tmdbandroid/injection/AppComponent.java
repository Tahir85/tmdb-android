package com.tj.tmdbandroid.injection;

import android.app.Application;

import com.tj.tmdbandroid.App;
import com.tj.tmdbandroid.injection.modules.ActivityModule;
import com.tj.tmdbandroid.injection.modules.AppModule;
import com.tj.tmdbandroid.injection.modules.NetModule;
import com.tj.tmdbandroid.injection.modules.RepositoryModule;
import com.tj.tmdbandroid.injection.modules.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,

        ActivityModule.class,
        RepositoryModule.class,
        ViewModelModule.class,
        NetModule.class,
        AppModule.class
})

public interface AppComponent extends AndroidInjector<App> {

    @Override
    void inject(App instance);

    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(Application application);
        AppComponent build();
    }

}
