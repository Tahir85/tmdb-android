package com.tj.tmdbandroid.injection.modules;

import com.tj.tmdbandroid.ui.MainActivity;
import com.tj.tmdbandroid.ui.MovieInfoActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    //activities

    @ContributesAndroidInjector
    abstract MainActivity contributesMainActivity();


    @ContributesAndroidInjector
    abstract MovieInfoActivity contributesMovieInfoActivity();

}
