package com.tj.tmdbandroid.injection.modules;
import com.tj.tmdbandroid.data_model.repository.MovieInfoRepository;
import com.tj.tmdbandroid.data_model.repository.MovieRepository;
import com.tj.tmdbandroid.executor.AppExecutors;
import com.tj.tmdbandroid.network.Webservice;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    public MovieRepository provideMovieRepository(AppExecutors executors, Webservice webservice){
        return new MovieRepository(executors, webservice);
    }


    @Provides
    @Singleton
    public MovieInfoRepository provideMovieInfoRepository(AppExecutors executors, Webservice webservice){
        return new MovieInfoRepository(executors, webservice);
    }
}
