package com.tj.tmdbandroid.injection.modules;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.tj.tmdbandroid.injection.util.ViewModelFactory;
import com.tj.tmdbandroid.injection.util.ViewModelKey;
import com.tj.tmdbandroid.vm.MovieInfoViewModel;
import com.tj.tmdbandroid.vm.MoviesViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory viewModelFactory);


    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel.class)
    abstract ViewModel bindMoviesViewModel(MoviesViewModel viewModel);


    @Binds
    @IntoMap
    @ViewModelKey(MovieInfoViewModel.class)
    abstract ViewModel bindMovieInfoViewModel(MovieInfoViewModel viewModel);



}
