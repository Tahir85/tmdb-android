package com.tj.tmdbandroid;

import com.tj.tmdbandroid.injection.AppComponent;
import com.tj.tmdbandroid.injection.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

public class App extends DaggerApplication {


    private static App instance;
    public static App getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent ac = DaggerAppComponent.builder().application(this).build();
        ac.inject(this);
        return ac;
    }
}
