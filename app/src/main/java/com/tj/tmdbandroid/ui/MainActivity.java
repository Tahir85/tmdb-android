package com.tj.tmdbandroid.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.tj.tmdbandroid.R;
import com.tj.tmdbandroid.adapters.MovieListAdapter;
import com.tj.tmdbandroid.databinding.MainActivityDataBinding;
import com.tj.tmdbandroid.injection.util.AutoFitGridLayoutManager;
import com.tj.tmdbandroid.injection.util.ViewModelFactory;
import com.tj.tmdbandroid.network.NetworkState;
import com.tj.tmdbandroid.vm.MoviesViewModel;

import java.util.Calendar;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity {

    private MainActivityDataBinding binding;

    @Inject
    ViewModelFactory viewModelFactory;

    private MoviesViewModel moviesViewModel;
    private MovieListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        //init the moviesViewModel
        moviesViewModel = ViewModelProviders.of(this, viewModelFactory).get(MoviesViewModel.class);


        //auto fill columns
        AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 500);
        binding.recyclerView.setLayoutManager(layoutManager);


        //movies adapter
        adapter = new MovieListAdapter(getApplicationContext());

        // When a new page is available, we call submitList() method of the PagedListAdapter class
        moviesViewModel.getMoviesLiveData().observe(this, pagedList -> {

            adapter.submitList(pagedList);
        });

        //show network activity as horizontal progressbar beneath the screen or error as toast.
        moviesViewModel.getNetworkState().observe(this, networkState -> {
            if(networkState != null) {
                handleProgressbar(networkState);
            }
        });

        binding.recyclerView.setAdapter(adapter);
    }


    /*
    * Show network activity on UI
    * */
    private void handleProgressbar(NetworkState networkState){
        if (networkState == null) return;
        if(networkState == NetworkState.LOADING){

            binding.pbProcessing.setVisibility(View.VISIBLE);
        }else if (networkState == NetworkState.COMPLETED) {

            binding.pbProcessing.setVisibility(View.INVISIBLE);
        } else if (networkState.getStatus() == NetworkState.Status.STATE_FAILED){

            binding.pbProcessing.setVisibility(View.INVISIBLE);
            Toast.makeText(this, networkState.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Menu mMenu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        mMenu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String query;
        switch (item.getItemId()) {

            case R.id.clear_filter:

                moviesViewModel.clearFilter();
                //show filter menu item
                MenuItem filterMenuItem = mMenu.findItem(R.id.action_filter);
                filterMenuItem.setVisible(true);
                //hide itself
                item.setVisible(false);

                return true;

            case R.id.popular:
                query = "sort_by=popularity.desc";
                filter(query);
                return true;


            case R.id.highest_rate:
                query = "certification_country=US&certification=R&sort_by=vote_average.desc";
                filter(query);

                return true;


            case R.id.popular_kids:
                query = "certification_country=US&certification.lte=G&sort_by=popularity.desc";
                filter(query);
                return true;

            case R.id.this_year:

                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                query = "primary_release_year=" + year;
                filter(query);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void filter(String filter){

        //hide filter item
        MenuItem filterMenuItem = mMenu.findItem(R.id.action_filter);
        filterMenuItem.setVisible(false);
        //show reset filter icon
        MenuItem clearFilterMenuItem = mMenu.findItem(R.id.clear_filter);
        clearFilterMenuItem.setVisible(true);

        moviesViewModel.filter(filter);
    }
}
