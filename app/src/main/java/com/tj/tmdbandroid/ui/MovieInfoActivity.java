package com.tj.tmdbandroid.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.squareup.picasso.Picasso;
import com.tj.tmdbandroid.R;
import com.tj.tmdbandroid.data_model.data.movie_info.MovieInfo;
import com.tj.tmdbandroid.databinding.MovieInfoActivityBinding;
import com.tj.tmdbandroid.injection.util.ViewModelFactory;
import com.tj.tmdbandroid.vm.MovieInfoViewModel;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public class MovieInfoActivity extends DaggerAppCompatActivity {

    private MovieInfoActivityBinding binding;
    private MovieInfoViewModel viewModel;

    @Inject
    ViewModelFactory viewModelFactory;
    private int movieId;

    private final static String ID_KEY = "id_key";

    public static Intent createIntent(int movieId, Context context){
        Intent intent = new Intent(context, MovieInfoActivity.class);
        intent.putExtra(ID_KEY, movieId);

        return intent;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            movieId = bundle.getInt(ID_KEY);
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_info);

        //init the MovieInfoViewModel
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieInfoViewModel.class);


        viewModel.getMovieInfo(movieId).observe(this, movieInfo -> {
            showData(movieInfo);
        });

    }

    private void showData(MovieInfo movieInfo){

        if (movieInfo == null) return;

        binding.pbProcessing.setVisibility(View.GONE);
        binding.contentLinearLayout.setVisibility(View.VISIBLE);

        Picasso.get().load(movieInfo.getBackdropUrl()).into(binding.backdropImageView);

        binding.titleTextView.setText(movieInfo.getTitle());
        binding.releaseDateTextView.setText(movieInfo.getReleaseDate());

        float rating = (float) movieInfo.getVoteAverage()*10 / 20;
        binding.ratingBar.setRating(rating);

        binding.overviewTextView.setText(movieInfo.getOverview());

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
